## _State Pattern_-CLI: Aufbau des Patterns und Navigationsstruktur

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110504024656041988</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/state-pattern-cli1</span>

> **tl/dr;** _(ca. 20 min Lesezeit): Am Beispiel eines Programms mit Kommandozeilenmenü wird das State-Pattern beispielhaft in Java implementiert: Eine Fingerübung, um das Pattern, Transitionen und die Elemente des UML-Zustandsdiagramms zu verinnerlichen. Im ersten Teil geht es zunächst um den Aufbau des Patterns selbst und die Grundstruktur._

Der Artikel ist Bestandteil einer mehrteiligen Reihe:

1. Aufbau des State-Patterns allgemein und grundlegende Struktur (dieser Artikel)
2. [Realisierung der Zustände als Singletons (per Java-ENUMS)](https://oer-informatik.de/state-pattern-cli2)
3. [Realisierung der Transitionen und deren Trigger](https://oer-informatik.de/state-pattern-cli3)
4. [Realisierung von Effekten und Guards von Transitionen](https://oer-informatik.de/state-pattern-cli4)

## Ausgangspunkt: Das UML-State-Diagramm

Vorneweg: wir sind uns einig: Java ist auch nicht meine erste Wahl für eine Konsolen-App. Sinn und Zweck dieses Tutorials ist nich, für einen bestehenden Anwendungsfall eine nutzbare App zu erstellen. Ziel ist vielmehr, mit einer kleinen Fingerübung einen Einblick zu gewinnen in das State-Pattern. Ausversehen nutzen wir dazu eine ganze Reihe Notationmittel der UML und tauchen ein in die OOP mit Java, Interfaces, Abstrakten Klassen, Lambda-Ausdrücken und Generics.

Wenn in einem User-Interface (egal ob CLI, GUI oder andere) unterschiedliche Menüs, Einträge oder Dialoge geöffnet sind, reagiert das System jeweils anders auf Eingaben. Es befindet sich also in unterschiedlichen Zuständen: bei einer Webapp macht es beispielsweise einen gravierenden Unterschied, ob der Zustand der App "als Nutzer angemeldet" oder "als Gast aktiv" ist. Daher lässt sich die Navigation in einem User-Interface sehr gut mit [UML-Zustandsdiagrammen](https://oer-informatik.de/uml-zustandsdiagramm) planen. Um das Programm einfach zu halten modellieren wir ein Kommandozeilen-Programmfragment, dass beispielhaft mit Java realisiert werden soll. Die Struktur ist bei _Graphischen Benutzeroberflächen_ (GUI) aber genauso.

Basis soll ein rudimentäres User-Interface sein, das für eine Adresse oder eine andere x-beliebige _Entity_ (Datenhaltungsklasse) einfache CRUD-Masken zum Erstellen (**C**reate), Lesen (**R**ead), Ändern (**U**pdate) und Löschen (**D**elete) bereitstellt. Wenn das Programm gestartet ist und ich angemeldet bin, erhalte ich eine Liste der eingetragenen Werte und von dort in die anderen Masken wechseln, die die Detailverarbeitung übernehmen sollen. Folgendes [UML-Zustandsdiagrammen](https://oer-informatik.de/uml-zustandsdiagramm) modelliert die geplante Navigation des User-Interfaces:

![Zustandsdiagramm der Kommendozeilen-Benutzerführung](plantuml/crud-ui-state-diagramm.png)

Es soll eine möglichst einfache Navigation umgesetzt werden: per Tastendruck wird ein Menüpunkt ausgewählt. Beispielsweise triggert `"a"` in jedem Zustand den Abbruch.  Es sind zunächst nicht allzu viele innere Aktionen modelliert, um das Beispiel klein zu halten. Wir wollen nur prototypisch die Realisierung dieser Zustandswechsel mit Java unter Zuhilfenahme des _State-Patterns_ zeigen.

## Allgemeine Formulierung des _State Pattern_

Das _State-Pattern_ ist eines der Gang-of-Four-Pattern und es ermöglicht es einem Objekt, sein Verhalten zu ändern, wenn sein interner Zustand sich ändert. 

Ein Beispiel: das Verhalten der Methode `reagiereAufAnstupsen()` ändert sich abhängig davon, ob das Objekt im Zustand `Wach` oder `Schlafend` ist.

Im _State Pattern_ realisiert wird das, in dem der Zustand des Objekts in eigene Klassen ausgelagert wird, die das Verhalten (also die Implementierung der Methoden) dann an den Zustand anpassen. 

![UML-Klassendiagramm, in dem das Verhalten der Klasse `UnterhaltungsKontext` über das Interface `State` in den Implementierungen von `State` mit Namen `Wach` und `Schlafend` realisiert wird](plantuml/state-pattern-classdiagramm.png)

Wie sich ein Objekt der Klasse `UnterhaltungsKontext` verhält, wenn die Methode `reagierenAufAnstupsen()` ausgeführt wird, bestimmt das Objekt, welches derzeit mit dem Attribut `zustand` referenziert wird. Im obigen Beispiel ist `zustand` lose gekoppelt über das Interface `State` und kann daher Instanzen von `Wach` oder `Schlafend` referenzieren. Sollte neben den individuellen Methoden `reagierenAufAnstupsen()` noch gemeinsames Verhalten von  `Wach` und `Schlafend` existieren, könnte man an Stelle des Interfaces auch eine abstrakte Klasse nutzen und hier gemeinsam genutzte Attribute und Methoden implementieren. In der Beschreibung der _Gang of Four_  wird eine abstrakte Klasse genutzt.

Den Instanzwechsel habe ich oben vereinfachend realisiert, in dem `wecken()` und `einschalfen()` jeweils neue Objekte von `Wach` und `Schlafend` erstellen - hier sollten natürlich bestehende Objekte weitergenutzt werden.

## Realisierung für die Kommandozeilen-Benutzeroberfläche

### Entwurf des Klassendiagramms für die konkrete Implementierung

Erster Schritt: Welche Zustände benötigen wir, welche Aktivitäten müssen bereitgestellt werden und was ist unser Kontext? Mithilfe des Zustandsdiagramms lassen sich die Zustandsklassen, der Kontext und das Interface entwerfen. Wir vereinfachen die Funktionalität zunächst stark: jeder Zustand hat je eine Aktivität, die bei Eintritt in den Zustand (`entry()`), während der Zustand aktiv ist (`doing()`) und bei Austritt aus dem Zustand (`exit()`) ausgeführt wird. Aus dem Zustandsdiagramm ergeben sich noch einige spezialisierte Aktivitäten einiger Zustandsklassen. Es entsteht folgendes UML-Klassendiagramm:

![Der Grundlegende Aufbau des _State-Patterns_ angepasst für das CLI](plantuml/crud-ui-classdiagramm-step01.png)

### Dreh- und Angelpunkt: die Kontextklasse `UIContext` und deren Zustandswechsel

Der Kontext ist das Objekt, dessen innerer Zustand das Verhalten ändert und deswegen ausgelagert wird. In unserem Fall ist das die Kommandozeilen-Benutzeroberfläche (UI).

Wir implementieren ein aggregiertes Zustandsobjekt `state` mit Setter, einen Konstruktor, die drei Aktivitäten `entry()`, `doing()`, `exit()`, deren Verhalten jeweils an den aktiven Zustand delegiert werden und eine `main()`-Methode, mit der wir das Programm starten und in der wir ein Objekt unserer Benutzeroberfläche erzeugen:

```java
public class UIContext {

    private UIStateInterface state;

    public UIContext() {}

    public void setState(UIStateInterface state) {
        this.state = state;
    }

    public static void main(String[] args) {
        UIContext myUI = new UIContext();
    }

    public void entry() {state.entry();}

    public void doing() {state.doing();}

    public void exit() {state.exit();}
}
```

Die Implementierung des Interfaces ist im obigen Klassendiagramm bereits vorgegeben:

```java
public interface UIStateInterface {
  public void entry();
  public void doing();
  public void exit();  
}
```

### Abstrakte Klasse mit Default-Implementierungen des Interfaces (Klasse `AbstractUIState`)

Zweiter Schritt: Wir machen uns das Leben ein wenig einfacher. Für den ersten Schritt ist es noch nicht erforderlich, die im Interface deklarieren Methoden (`entry()`, `doing()`, `exit()`) individuell zu implementieren. Es bietet sich daher an, eine abstrakte Klasse mit default-Implementierungen voran zu schalten:

![Die vorgeschaltete abstrakte Klasse AbstractUIState](plantuml/crud-ui-classdiagramm-step02.png)

Zunächst werden wir noch keine Aktivitäten für `entry()`, `doing()` und `exit()` definieren, sondern lediglich über Ausgaben prüfen, welche Zustände jeweils genutzt werden.

```java
public abstract class AbstractUIState implements UIStateInterface {

  public void entry() {
      System.out.println("<<< Entering state " + this.getClass().getSimpleName());
  }

  public void doing() {
      System.out.println("--- Being in state " + this.getClass().getSimpleName());
  }

  public void exit() {
      System.out.println(">>> Leaving state " + this.getClass().getSimpleName());
} }
```

Wir nutzen dazu das _Java-Reflection-Feature_: Java bietet Klassen und Methoden an, die Meta-Informationen über die aktuellen Objekte, liefern - `this.getClass().getSimpleName()` beispielsweise den Namen der genutzten Klasse. Wir können uns also ausgeben lassen, welche Spezialisierung der Klasse gerade aktiv ist, ohne den Code in den Spezialisierungen implementieren zu müssen.

### Die spezialisierten Zustandsklassen

Die spezialisierten Zustandsklassen sollen hier nur rudimentär implementiert werden. Einen großen Teil der Logik erben Sie bereits von der `AbstractUIState`-Klasse, sodass lediglich abweichende Implementierungen oder spezielle Methoden, die die Elternklasse nicht hatte, ausimplementiert werden müssen. Beispielhaft sei dies an den Zuständen `AnmeldenUI` und `ListUI` gezeigt, die jeweils nur eine zusätzliche Methode
(`login()` bzw. `abmelden()`) neu implementieren.

```java
public class AnmeldenUI extends AbstractUIState {

    public void login(){
        System.out.println("eingeloggt.");
}   }
```

```java
public class ListUI extends AbstractUIState {

    public void abmelden(){
        System.out.println("abgemeldet");
}   }
```

### Der erste Test

Damit wäre das _State-Pattern_ grundsätzlich fertig implementiert und wir können ausprobieren, ob der Zustandswechsel auch zu einem Verhaltenswechsel führt. In der `main()` Methode hatten wir bereits ein Objekt unseres Kontexts erstellt. Wir müssen jetzt nur per Setter-Methode den Zustand ändern und einige Kontext-Methoden aufrufen, um zu überprüfen, dass die Implementierung der jeweiligen Zustandsklassen genutzt wird.

```java
public static void main(String[] args) {
    UIContext myUI = new UIContext();

    //Erstelle einen inneren Zustand des Kontexts:
    myUI.setState(new AnmeldenUI());

    //Überprüfen des Verhaltens:
    myUI.entry();
    myUI.doing();
    myUI.exit();

    //Ändere den inneren Zustand des Kontexts:
    myUI.setState(new ListUI());

    //Überprüfen des Verhaltens:
    myUI.entry();
    myUI.doing();
}
```
Wie erwartet delegiert das Kontext-Objekt die Abarbeitung der Methoden `entry()`, `doing()` und `exit()` an das jeweilige Zustandsobjekt. Die rudimentäre Ausgabe des Programms verrät uns, welche Klasse sich jeweils um die Methoden gekümmert hat:

```
<<< Entering state AnmeldenUI
--- Being in state AnmeldenUI
>>> Leaving state AnmeldenUI
<<< Entering state ListUI
--- Being in state ListUI
```

Nachdem die Grundzüge funktionieren können wir uns im Folgenden um eine Umgruppierung der einzelnen Zustandsklassen kümmern.

[Weiter in Teil 2](https://oer-informatik.de/state-pattern-cli2)


