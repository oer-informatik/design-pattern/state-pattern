package de.csbme.ifaxx.uistate;

public enum UIStateEnum {
    ANMELDEN_UI(new AnmeldenUI()),
    LIST_UI(new ListUI()),
    CREATE_DETAIL_UI(new CreateDetailUI()),
    READ_DETAIL_UI(new ReadDetailUI()),
    UPDATE_DETAIL_UI(new UpdateDetailUI()),
    DELETE_DETAIL_UI(new DeleteDetailUI());

    private final UIStateInterface uiObject;

    UIStateEnum(UIStateInterface uiObject) {
        this.uiObject = uiObject;
    }

    public UIStateInterface getUIState() {
        return this.uiObject;
    }

    public static UIStateEnum getUIStateEnum(Object usedObject) {
        if (usedObject instanceof AnmeldenUI) {return ANMELDEN_UI;}    
        if (usedObject instanceof ListUI) {return LIST_UI;}
        if (usedObject instanceof ReadDetailUI) {return READ_DETAIL_UI;}
        if (usedObject instanceof CreateDetailUI) {return CREATE_DETAIL_UI;}
        if (usedObject instanceof UpdateDetailUI) {return UPDATE_DETAIL_UI;}
        if (usedObject instanceof DeleteDetailUI) {return DELETE_DETAIL_UI;}
        return null;
    }
  }