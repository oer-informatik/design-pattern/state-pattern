package de.csbme.ifaxx.uistate;

import java.util.function.Consumer;

public class Transition<T>{
        private UIStateEnum target;
        private String trigger;
        private String menueItem;
        private Consumer<T> effect;

        
        public Consumer<T> getEffect() {return effect;}

        public Transition(UIStateEnum target, String trigger, String menueItem, Consumer<T> effect){
            this.target = target;
            this.trigger=trigger;
            this.menueItem=menueItem;
            this.effect = effect;
        }

        public UIStateEnum getTarget() {return target;}

        public String getTrigger() {return trigger;}

        public String getMenueItem() {return menueItem;}

        public String toString(){
            return "'"+getTrigger()+"': "+getMenueItem()+" ("+getTarget()+")";
        }
    }