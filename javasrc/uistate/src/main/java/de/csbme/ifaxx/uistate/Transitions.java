package de.csbme.ifaxx.uistate;

import java.util.ArrayList;
import java.util.function.Consumer;

public class Transitions<T> {
    private UIStateEnum source;

    public Transitions(UIStateEnum source){
        this.source = source;
    }

    private ArrayList<Transition<T>> transitionList = new ArrayList<>();

    public void addTransition(UIStateEnum target, String trigger, String menueItem, Consumer effect){
        transitionList.add(new Transition<T>(target, trigger, menueItem, effect));
    }

    public Transition<T> findTransition(String actualTrigger){
        System.out.println("Prüfe, ob der Trigger <"+actualTrigger+"> eine Transition auslöst:");
        for(Transition<T> t : transitionList){
            System.out.println(" - Prüfe Transition "+t.getTarget().name()+" mit Trigger <" + t.getTrigger()+">");
            if (t.getTrigger().equals(actualTrigger)){
                System.out.println(" ==> Transition zu "+t.getTarget().name()+" gefunden! (Weitere Suche wird gestoppt)");
                return t;
        }   }
        return null;
    }

    @SuppressWarnings("unchecked")
    public UIStateEnum transist(Transition transition){

        if ((transition!= null) && (transition.getTarget() != null)){
            if (!(transition.getEffect() == null)){
                Consumer<T> effect = transition.getEffect();
                System.out.println("--- Führe Event auf " + source.name() + " aus: ---");
                effect.accept((T) source.getUIState());
                System.out.println("--- Event auf " + source.name() + " beendet ---");
            }
            source.getUIState().exit();
            transition.getTarget().getUIState().entry();
            return transition.getTarget();
        }else{
            System.out.println("Keine Transition gefunden! Bleibe bei "+source.getUIState().getClass().getSimpleName());
            return source;
        }
    }

    public String getMenue(){
        String menue = "";
        for(Transition<T> t : transitionList){
            menue += t.toString()+"\n";
        }
        return menue;
    }
}