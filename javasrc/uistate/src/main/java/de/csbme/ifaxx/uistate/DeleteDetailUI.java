package de.csbme.ifaxx.uistate;

public class DeleteDetailUI extends AbstractUIState {

    public void abbruch(){
        System.out.println("Abbruch");
    }

    public void ok(){
        System.out.println("OK");
    }

    public void initializeTransitions() {
        super.initializeTransitions(new Transitions<DeleteDetailUI>(UIStateEnum.getUIStateEnum(this)));
        this.addTransition(UIStateEnum.LIST_UI, "a", "Liste ausgeben", (DeleteDetailUI aUI) -> {aUI.abbruch();});
        this.addTransition(UIStateEnum.LIST_UI, "o", "Eintrag speichern", (DeleteDetailUI aUI) -> {aUI.ok();});
    }
}