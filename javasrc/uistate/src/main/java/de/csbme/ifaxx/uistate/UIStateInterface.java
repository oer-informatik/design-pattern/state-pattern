package de.csbme.ifaxx.uistate;

public interface UIStateInterface {
  public void entry();
  public void doing();
  public void exit();
  public UIStateEnum handleTriggerEvent(String trigger);
  public <T extends UIStateInterface>  void initializeTransitions();
  public void showMenue();
}