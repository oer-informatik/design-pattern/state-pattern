package de.csbme.ifaxx.uistate;

public class ListUI extends AbstractUIState {

    public void abbruch(){
        System.out.println("abgemeldet");
    }   
    public void initializeTransitions() {
        super.initializeTransitions(new Transitions<ListUI>(UIStateEnum.getUIStateEnum(this)));
        this.addTransition(UIStateEnum.ANMELDEN_UI, "a", "Abmelden", (ListUI aUI) -> {aUI.abbruch();});
        this.addTransition(UIStateEnum.CREATE_DETAIL_UI, "c", "Neuen Eintrag erstellen", (ListUI aUI) -> {});
        this.addTransition(UIStateEnum.READ_DETAIL_UI, "r", "Details anzeigen", (ListUI aUI) -> {});
        this.addTransition(UIStateEnum.UPDATE_DETAIL_UI, "u", "Eintrag ändern", (ListUI aUI) -> {});
        this.addTransition(UIStateEnum.DELETE_DETAIL_UI, "d", "Eintrag löschen", (ListUI aUI) -> {});
    }
}