package de.csbme.ifaxx.uistate;

public class ReadDetailUI extends AbstractUIState {

    public void abbruch(){
        System.out.println("Abbruch");
    }

    public void ok(){
        System.out.println("OK");
    }

    public void initializeTransitions() {
        super.initializeTransitions(new Transitions<ReadDetailUI>(UIStateEnum.getUIStateEnum(this)));
        this.addTransition(UIStateEnum.LIST_UI, "a", "Liste ausgeben", (ReadDetailUI aUI) -> {aUI.abbruch();});
    }
}