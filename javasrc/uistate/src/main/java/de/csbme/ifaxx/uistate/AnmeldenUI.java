package de.csbme.ifaxx.uistate;

public class AnmeldenUI extends AbstractUIState {

    public void abbruch(){
        System.exit(0);
    }
    public void initializeTransitions(){
        super.initializeTransitions(new Transitions<AnmeldenUI>(UIStateEnum.getUIStateEnum(this)));
        this.addTransition(UIStateEnum.LIST_UI, "l", "Anmelden");
        this.addTransition(UIStateEnum.LIST_UI, "a", "Anmelden", (AnmeldenUI aUI) -> {aUI.abbruch();} );
    }
    
    public void login(){
        System.out.println("eingeloggt.");
    }
}