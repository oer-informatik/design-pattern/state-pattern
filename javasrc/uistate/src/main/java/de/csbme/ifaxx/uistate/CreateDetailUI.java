package de.csbme.ifaxx.uistate;

public class CreateDetailUI extends AbstractUIState {

    public void abbruch(){
        System.out.println("Abbruch");
    }

    public void ok(){
        System.out.println("OK");
    }

    public void initializeTransitions() {
        super.initializeTransitions(new Transitions<CreateDetailUI>(UIStateEnum.getUIStateEnum(this)));
        this.addTransition(UIStateEnum.LIST_UI, "a", "Liste ausgeben", (CreateDetailUI aUI) -> {aUI.abbruch();});
        this.addTransition(UIStateEnum.READ_DETAIL_UI, "o", "Details anzeigen",(CreateDetailUI aUI) -> {aUI.ok();} );
    }
}