package de.csbme.ifaxx.uistate;

public class UpdateDetailUI extends AbstractUIState {

    public void abbruch(){
        System.out.println("Abbruch");
    }

    public void ok(){
        System.out.println("OK");
    }

    public void initializeTransitions() {
        super.initializeTransitions(new Transitions<UpdateDetailUI>(UIStateEnum.getUIStateEnum(this)));
        this.addTransition(UIStateEnum.READ_DETAIL_UI, "a", "Details anzeigen", (UpdateDetailUI aUI) -> {aUI.abbruch();});
        this.addTransition(UIStateEnum.READ_DETAIL_UI, "o", "Ok, Speichern", (UpdateDetailUI aUI) -> {aUI.ok();});
        this.addTransition(UIStateEnum.DELETE_DETAIL_UI, "d", "Eintrag löschen", (UpdateDetailUI aUI) -> {});
    }
}