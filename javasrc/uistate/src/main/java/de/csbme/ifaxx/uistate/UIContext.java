package de.csbme.ifaxx.uistate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UIContext {

    private UIStateEnum uiStateEnum;

    public void setUiStateEnum(UIStateEnum newUiStateEnum) {
                this.uiStateEnum = newUiStateEnum;
    }

    public UIContext() {}


    public static void main(String[] args) {
        for (UIStateEnum nextUiStateEnum : UIStateEnum.values()) {
            nextUiStateEnum.getUIState().initializeTransitions();
        }
        UIContext myUI = new UIContext();
        myUI.setUiStateEnum(UIStateEnum.ANMELDEN_UI);
        myUI.uiStateEnum.getUIState().showMenue();

        for(;;){
           myUI.loop();
        }
    }

    public void entry() {
        uiStateEnum.getUIState().entry();
        uiStateEnum.getUIState().showMenue();
    }

    public void doing() {
        uiStateEnum.getUIState().doing();
    }

    public void exit() {
        uiStateEnum.getUIState().entry();
        }

        public void loop(){
            String keyStroke = readln();
            UIStateEnum uistate = handleTriggerEvent(keyStroke);
            setUiStateEnum(uistate);
        }

        public UIStateEnum handleTriggerEvent(String keyStroke){
            return uiStateEnum.getUIState().handleTriggerEvent(keyStroke);
        }

        private String readln() {
            String eingabe ="";
            InputStreamReader inReader = new InputStreamReader(System.in);
        
            try{
                BufferedReader buffReader = new BufferedReader(inReader);
                System.out.println("UI-Menu (Eingabe mit [ENTER] beenden)>"); //Eine Art Eingabeaufforderung - muss natürlich nicht sein
                while ("".equals(eingabe)){
                  eingabe = buffReader.readLine();
                }
            } catch (IOException ex) {
                System.out.println("Es ist ein Fehler aufgetreten: "+ ex.getMessage());
            }
        
            return eingabe;
        }
}

