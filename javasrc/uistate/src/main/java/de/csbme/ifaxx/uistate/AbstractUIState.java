package de.csbme.ifaxx.uistate;

import java.util.function.Consumer;

public abstract class AbstractUIState implements UIStateInterface {

  public void entry() {
      System.out.println("<<< Entering state " + this.getClass().getSimpleName());
      showMenue();
  }

  public void doing() {
      System.out.println("--- Being in state " + this.getClass().getSimpleName());
  }

  public void exit() {
      System.out.println(">>> Leaving state " + this.getClass().getSimpleName());
    } 

   private Transitions<? extends UIStateInterface> transitions;

    public void initializeTransitions(Transitions<? extends UIStateInterface> myTransisitons) {
        transitions = myTransisitons;
    }

    public void addTransition(UIStateEnum target, String trigger, String menueItem) {
        this.transitions.addTransition(target, trigger, menueItem,  o->{System.out.println("Keine Aktivität für diesen Effekt hinterlegt.");});
}

    public void addTransition(UIStateEnum target, String trigger, String menueItem, Consumer<? extends UIStateInterface> effect) {
            this.transitions.addTransition(target, trigger, menueItem, effect);
    }

    public UIStateEnum handleTriggerEvent(String trigger) {
        Transition<? extends UIStateInterface> foundTransition = this.transitions.findTransition(trigger);
        return this.transitions.transist(foundTransition);
    }

    public void showMenue(){
        int menueheader = 60;
        System.out.println();
        System.out.println("#".repeat( menueheader));
        int border = (menueheader - 2 - this.getClass().getSimpleName().length())/2;
        System.out.println("#".repeat(border) + " "+this.getClass().getSimpleName() +" " + "#".repeat(border));
        System.out.println("#".repeat( menueheader));
        System.out.println(transitions.getMenue());
    }
}