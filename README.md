# Das _State-Pattern_ (Zustandsmuster)

* [Realisierung des State-Pattern für eine Konsolen Benutzeroberfläche (Teil 1)](https://oer-informatik.gitlab.io/design-pattern/state-pattern/state-pattern-cli1.html) [(als PDF)](https://oer-informatik.gitlab.io/design-pattern/state-pattern/state-pattern-cli1/state-pattern-cli1.pdf)

* [Realisierung des State-Pattern für eine Konsolen Benutzeroberfläche (Teil 2: Kapselung mit ENUM-Klasse)](https://oer-informatik.gitlab.io/design-pattern/state-pattern/state-pattern-cli2.html) [(als PDF)](https://oer-informatik.gitlab.io/design-pattern/state-pattern/state-pattern-cli1/state-pattern-cli2.pdf)

# Infos zur genutzten gitlab CI und deren Nachnutzung:
Die HTML und PDF-Dateien wurden mit Hilfe der Konfiguraiton der [TIBHannover](https://gitlab.com/TIBHannover/oer/course-metadata-test/) für die CI-Pipeline von Gitlab erstellt.
Die Vorlage findet sich hier: [https://gitlab.com/TIBHannover/oer/course-metadata-test/](https://gitlab.com/TIBHannover/oer/course-metadata-test/).
